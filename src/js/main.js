
var roomsSlider = document.querySelector('section.rooms');
var roomSliderItemClass ='room-name-item';

roomsSlider.addEventListener("click", function(e) {
  var target = e.target;
  var currentSlideTab, currentText, currentImgSlider, nextImgSlider, nextText, currentRoomName,
    currentImg, currentImgSlideNum, nextImgSlideNum, nextImg, prevImg, prevImgSlideNum;


  if (target.classList.contains(roomSliderItemClass) && !target.classList.contains('active')) {

    currentSlideTab = document.querySelector('.room-name-item.active');
    currentRoomName = currentSlideTab.getAttribute('data-roomname');

    targetRoomName = target.getAttribute('data-roomname');

    currentText = document.querySelector('.text-wrapper[data-roomname="'+currentRoomName+'"]');
    currentImgSlider = document.querySelector('.img-slider[data-roomname="'+currentRoomName+'"]');

    nextText = document.querySelector('.text-wrapper[data-roomname="'+targetRoomName+'"]');
    nextImgSlider = document.querySelector('.img-slider[data-roomname="'+targetRoomName+'"]');

    currentSlideTab.classList.remove('active');
    target.classList.add('active');

    currentText.classList.add('fadeOut');
    currentImgSlider.classList.add('fadeOut');
    nextText.classList.add('fadeIn');
    nextImgSlider.classList.add('fadeIn');

     if (nextImgSlider.classList.contains('fadeIn')) {
       setTimeout(function () {
         currentText.classList.remove('active');
         currentImgSlider.classList.remove('active');
         nextText.classList.add('active');
         nextImgSlider.classList.add('active');
       }, 700);

      setTimeout(function () {
        currentText.classList.remove('fadeOut');
        currentImgSlider.classList.remove('fadeOut');
        nextText.classList.remove('fadeIn');
        nextImgSlider.classList.remove('fadeIn');
       }, 700);
     }

  }

  if (target.classList.contains('arr-front')) {

    currentSlideTab = document.querySelector('.room-name-item.active');
    currentRoomName = currentSlideTab.getAttribute('data-roomname');

    currentImgSlider = document.querySelector('.img-slider[data-roomname="' + currentRoomName + '"]');

    currentImg = document.querySelector('.img-slider[data-roomname="' + currentRoomName + '"] img.active');
    currentImgSlideNum = currentImg.getAttribute('data-slidenum');
    nextImgSlideNum = ((Number(currentImgSlideNum) + 1) > currentImgSlider.children.length) ? 1 : (Number(currentImgSlideNum) + 1);
    nextImg = document.querySelector('.img-slider[data-roomname="' + currentRoomName + '"] img[data-slidenum="' + (nextImgSlideNum) + '"]');

    currentImg.classList.add('fadeOut');
    nextImg.classList.add('fadeIn');
    if (nextImg.classList.contains('fadeIn')) {
      setTimeout(function () {
        currentImg.classList.remove('active');
        nextImg.classList.add('active');
      }, 700);
      setTimeout(function () {
        currentImg.classList.remove('fadeOut');
        nextImg.classList.remove('fadeIn');
      }, 700);
    }
  }

  if (target.classList.contains('arr-back')) {

    currentSlideTab = document.querySelector('.room-name-item.active');
    currentRoomName = currentSlideTab.getAttribute('data-roomname');

    currentImgSlider = document.querySelector('.img-slider[data-roomname="' + currentRoomName + '"]');

    currentImg = document.querySelector('.img-slider[data-roomname="' + currentRoomName + '"] img.active');
    currentImgSlideNum = currentImg.getAttribute('data-slidenum');
    prevImgSlideNum = ((Number(currentImgSlideNum) - 1) < 1) ? currentImgSlider.children.length : (Number(currentImgSlideNum) - 1);
    prevImg = document.querySelector('.img-slider[data-roomname="' + currentRoomName + '"] img[data-slidenum="' + (prevImgSlideNum) + '"]');

    currentImg.classList.add('fadeOut');
    prevImg.classList.add('fadeIn');
    if (prevImg.classList.contains('fadeIn')) {
      setTimeout(function () {
        currentImg.classList.remove('active');
        prevImg.classList.add('active');
      }, 700);
      setTimeout(function () {
        currentImg.classList.remove('fadeOut');
        prevImg.classList.remove('fadeIn');
      }, 700);
    }
  }
});